#define LINUX

#include "mp1_given.h"
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/gfp.h>
#include <linux/slab.h>
#include <linux/list.h> 
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/spinlock.h>
#include <linux/workqueue.h>
#include <linux/timer.h>
#include <asm/uaccess.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("23");
MODULE_DESCRIPTION("CS-423 MP1");

typedef struct list_node{
	struct list_head list;
	pid_t pid;
	unsigned long cpu_time;
}list_node;

typedef struct {
    struct work_struct work;
    int id;
} my_work_t;


static struct proc_dir_entry *reg_dir;
static struct proc_dir_entry *status_entry;
static spinlock_t lock;
static struct timer_list my_timer;
static struct hrtimer htimer;
static ktime_t kt_periode;
static struct workqueue_struct *my_wq;
static int idx = 0;
LIST_HEAD(proc_list);


static void update_cpu_time(void) {
    
	unsigned long flags;
	unsigned long cpu_use;
	struct list_head *pos;
	struct list_node *tmp;
	
	spin_lock_irqsave(&lock,flags);
	list_for_each(pos,&proc_list){
		tmp = list_entry(pos,struct list_node,list);
		if(!get_cpu_use(tmp->pid, &cpu_use)){
			tmp->cpu_time = jiffies_to_msecs(cputime_to_jiffies(cpu_use));
			printk("pid: %u, time: %lu\n", tmp->pid,tmp->cpu_time);
		}
		else{
			printk("process %u does not exit!\n",tmp->pid);
		}
	}
	spin_unlock_irqrestore(&lock,flags);
}


static void my_wq_function(struct work_struct *work) {
	
	my_work_t* my_work = (my_work_t *)work;

	//printk("work: %u\n", my_work->id);

	update_cpu_time();

	kfree(my_work);

	return;
}

/*
void my_timer_callback(unsigned long data) {
    printk("my_timer_callback called (%ld).\n", jiffies);
}
*/

static enum hrtimer_restart timer_function(struct hrtimer * timer)
{
	// @Do your work here. 
	unsigned long flags;
	my_work_t* my_work = (my_work_t*)kmalloc(sizeof(my_work_t),GFP_KERNEL);
	INIT_WORK(&my_work->work, my_wq_function);
	my_work->id = idx++;

	spin_lock_irqsave(&lock,flags);
	queue_work(my_wq, &my_work->work);
	spin_unlock_irqrestore(&lock,flags);

	hrtimer_forward_now(timer, kt_periode);

	//printk("my_timer_callback called 5.\n");
	return HRTIMER_RESTART;
}

static void timer_init(void)
{
	kt_periode = ktime_set(5, 0); //seconds,nanoseconds
	hrtimer_init (& htimer, CLOCK_REALTIME, HRTIMER_MODE_REL);
	htimer.function = timer_function;
	hrtimer_start(& htimer, kt_periode, HRTIMER_MODE_REL);
}

static void timer_cleanup(void)
{
	hrtimer_cancel(& htimer);
}

static ssize_t mp1_read (struct file *file, char __user *buffer, size_t count, loff_t *data){ 


	ssize_t copied = 0; 
	char * buf = (char *) kmalloc(count,GFP_KERNEL);
	struct list_head* pos;
	struct list_node* tmp;
	unsigned long flags;

	printk("read locked\n");
	//lock
	spin_lock_irqsave(&lock,flags);	
	list_for_each(pos,&proc_list){
		//printk("reading pid: %u, time: %lu\n", tmp->pid,tmp->cpu_time);
		tmp = list_entry(pos,struct list_node,list);
		copied+=sprintf(buf+copied,"PID%u: %lu\n",tmp->pid,tmp->cpu_time);
	}
	spin_unlock_irqrestore(&lock,flags);
	//unlock
	printk("read unlocked\n");
	buf[copied] = 0;

	printk("read offset: %d, read copied: %d\n",*data,copied);

	if(*data >= copied){
		kfree(buf);
		return 0;
	}

	if(copy_to_user(buffer, buf, copied)){
		kfree(buf);
		printk("Error in read\n");
		return -EINVAL;
	}
	*data+=copied;

	kfree(buf);

	return copied;
} 

static ssize_t mp1_write (struct file *file, const char __user *buffer, size_t count, loff_t 
*data){ 

	printk("write offset: %d\n",*data);
	char * buf = (char *) kmalloc(count,GFP_KERNEL);
	unsigned long flags;
	int pid;
	struct list_node* tmp;

	if(copy_from_user(buf,buffer, count)){
		kfree(buf);
		return -EFAULT;
	}	
	buf[count] = 0;
	sscanf(buf,"%u",&pid);

	tmp = (struct list_node*)kmalloc(sizeof(struct list_node),GFP_KERNEL);
	tmp->pid = pid;
	tmp->cpu_time = 0;	
	INIT_LIST_HEAD(&tmp->list);

	printk("write locked\n");

	spin_lock_irqsave(&lock,flags);
	list_add_tail(&tmp->list, &proc_list);
	spin_unlock_irqrestore(&lock,flags);		

	printk("write unlocked\n");
	
	kfree(buf);	

	return count;
} 

struct file_operations mp1_fops = {
	.owner = THIS_MODULE,
	.read= mp1_read,
	.write= mp1_write,
};


#define DEBUG 1

// mp1_init - Called when module is loaded

int __init mp1_init(void)
{
	struct list_node* proc_list_head;

	#ifdef DEBUG
	printk(KERN_ALERT "MP1 MODULE LOADING\n");
	#endif
	// Insert your code here ...

	reg_dir = proc_mkdir("mp1",NULL);
	
	if(!reg_dir)
	{
		printk(KERN_INFO "directory creation failed\n");
		return -ENOMEM;
	}
	printk(KERN_INFO "proc dir successfully created\n");

	status_entry = proc_create("status",0666,reg_dir,&mp1_fops);

   
	if(!status_entry)
	{
		remove_proc_entry("mp1",NULL);		
		printk(KERN_INFO "proc entry creation failed\n");
		return -ENOMEM;
	}
	printk(KERN_INFO "file successfully created\n");

	//initialize the head of this linked list

	proc_list_head = (struct list_node*)kmalloc(sizeof(struct list_node),GFP_KERNEL);
	INIT_LIST_HEAD(&proc_list_head->list);	
	proc_list_head->pid = (pid_t)current->pid;
	proc_list_head->cpu_time = 0;	
	printk("init %d\n",current->pid);
	list_add_tail(&proc_list_head->list, &proc_list);

	//printk("list is empty? %s\n",(list_empty(&proc_list)!=0)?"Yes!":"No!");

	//initialize spinlock
	spin_lock_init(&lock);
	printk(KERN_INFO "lock initialized\n");

	//initialize timer
/*	setup_timer(&my_timer, my_timer_callback, 0);
	int ret = mod_timer(&my_timer, jiffies + msecs_to_jiffies(5000));
	if (ret) printk("timer setting failed\n");
*/
	timer_init();
	printk(KERN_INFO "timer initialized\n");

	my_wq = create_workqueue("my_queue");
	if(!my_wq) 
		printk(KERN_INFO "workqueue creation failed\n");
	else
		printk(KERN_INFO "workqueue initialized\n");

	printk(KERN_ALERT "MP1 MODULE LOADED\n");
	return 0;   
}

// mp1_exit - Called when module is unloaded
void __exit mp1_exit(void)
{
		
	struct list_head* pos, *n;
	struct list_node* tmp;

	#ifdef DEBUG
	printk(KERN_ALERT "MP1 MODULE UNLOADING\n");
	#endif
	// Insert your code here ...

	remove_proc_entry("status",reg_dir);
	remove_proc_entry("mp1",NULL);

	list_for_each_safe(pos,n,&proc_list){
		tmp = list_entry(pos,list_node,list);
		//printk(KERN_ALERT "Remove element %d: time %d\n",tmp->pid,tmp->cpu_time);				
		list_del(&tmp->list);
		//printk(KERN_ALERT "Node Deleted\n");	
		kfree(tmp);
	}

	list_del(&proc_list);
	printk(KERN_INFO "list successfully destroyed\n");

	int ret = del_timer_sync(&my_timer);
	if (ret) 
		printk("The timer is still in use...\n");
	else
		printk(KERN_INFO "timer successfully destroyed\n");


	flush_workqueue(my_wq);
	destroy_workqueue(my_wq);
	printk(KERN_INFO "workqueue successfully destroyed\n");

	timer_cleanup();

	printk(KERN_ALERT "MP1 MODULE UNLOADED\n");
}

// Register init and exit funtions
module_init(mp1_init);
module_exit(mp1_exit);
