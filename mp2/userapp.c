#include "userapp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
static int buffer_size = 1024;

void dispList(FILE* fp, char* buf){
	fseek(fp,0,SEEK_SET);
	while(fgets(buf,buffer_size,fp) != NULL){
		printf("%s",buf);
	}
}
unsigned long long fac(unsigned long long in)
{
	if(in > 1) {
		return in * fac(in-1);
	}
	else {
		return 1;
	}
}
int main(int argc, char* argv[])
{   
	// bool bContinue = true


	;
	pid_t pid = getpid();
	FILE* fp = fopen("/proc/mp1/status","r+");
	size_t count;
	int i, j = 0;
	int in_pid;
	struct timeval tval;
	char strtime[255];

	time_t second;
	struct timeval tval_actual_runtime;
	time_t second_actual_runtime;

	fflush(fp);
	char buf[buffer_size];

	clock_t start, end;
	double cpu_time_used;

	if(fp == NULL){
		printf("The module is not loaded\n");
		return -1;
	}	

	rewind(fp);
	gettimeofday(&tval, NULL);
	second = tval.tv_sec;
	strftime(strtime, 255, "%D %T", localtime(&second));
	while(j < 25) {
		start = clock();
		gettimeofday(&tval, NULL);
		second = tval.tv_sec;
		strftime(strtime, 255, "%D %T", localtime(&second));
		printf("Process %d woke at: %s.%06ld\n", pid, strtime, tval.tv_usec);
		for (i = 0; i < 15000; i++) {
			fac(20LLU);
			// if(!bContinue){
			// 	break;
			// }
		}
	j++;
	gettimeofday(&tval_actual_runtime, NULL);
	second_actual_runtime = tval_actual_runtime.tv_sec;
	strftime(strtime, 255, "%D %T", localtime(&second_actual_runtime));
	printf("Took %06ld\n", (tval_actual_runtime.tv_usec - tval.tv_usec));
	count = fprintf(fp, "Y %d", pid);
	fflush(fp);
	end = clock();
	cpu_time_used = ((double)(end - start))/CLOCKS_PER_SEC;
	printf("while loop took %f seconds to execute \n", cpu_time_used);

	}


	printf("initialized list\n");

	while(fgets(buf,buffer_size,fp) != NULL){
		printf("%s",buf);
		//printf("1st read: %lu bytes scanned:%s\n",strlen(buf),buf);
	}

 	printf("register process %u\n",pid);
	sprintf(buf,"%u",pid);
	fputs(buf,fp);

	printf("current list\n");

	while(fgets(buf,buffer_size,fp) != NULL){
		printf("%s",buf);
		//printf("2nd read: %lu bytes scanned:%s\n",strlen(buf),buf);
	}

	system("sleep 5");
	printf("\n");
	printf("cpu time updated\n");

	fseek(fp,0,SEEK_SET);
	while(fgets(buf,buffer_size,fp) != NULL){
		printf("%s",buf);
		//printf("2nd read: %lu bytes scanned:%s\n",strlen(buf),buf);
	}


	printf("\n");
	printf("please enter a process:\n");
	fgets(buf,buffer_size,stdin);
	fputs(buf,fp);
	dispList(fp,buf);

	system("sleep 6");
	printf("\n");
	printf("cpu time updated\n");
	dispList(fp,buf);


	fclose(fp);	

	return 0;
}
