#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <queue>
#include <thread>
#include <chrono>
#include <cstring>
#include "sys/times.h"
#include "hardware_monitor.cpp"
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <algorithm>
//#include <thread>
#include <sstream>
#include <iterator>
#include <mutex>
#include <stdio.h>
#include "server.cpp"
#include "get_ip.cpp"
#include "send.cpp"
#include "math.h"
// using namespace std;
using std::string;
using std::exception;
using std::stringstream;
using std::cin;
using std::pair;
using std::mutex;
using std::thread;

#define MAXBUFLEN 1000

double currentCpuUsg = 0;
std::queue<Job> job_queue;
double throttling_value = 0.7;
int pending_jobs = -1;
int r_pending_jobs = -1;
double r_throttling_value = -1;



static bool READY = false;
mutex mutex_job_queue;
mutex mutex_computed_jobs;
mutex mutex_count;
int batch = 1000;
int monitor_interval = 1000;
int adaptor_interval = 5000;
int state_manager_interval = 1000;
int network_interval = 5000;
int diff_thres = 80;
static const std::string big_ip = "172.22.154.47";
static const std::string small_ip = "172.22.154.48";
bool REMOTE = false;
static double rst[1024 * 1024 * 4];

std::queue<Job> computed_jobs;

static std::string self_ip;
static std::string remote_ip;
static int count_jobs = 0;

//static int count_serv = 0;
// int SENDER_JOB_THRES = 160;
// int RECEIVER_JOB_THRES = 240; need to add threshold?


template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

ssize_t cir_read(int sockfd, char *user_buf, size_t n){
    size_t nleft = n;
    ssize_t nread;
    char * buf = user_buf;

    while(nleft > 0){
        if ((nread = read(sockfd, buf, nleft)) < 0) {
            if (errno = EINTR) {
                nread = 0;
            }
            else 
                return -1;
        }
        else if (nread == 0) 
            break;
        nleft -= nread;
        buf += nread;
    }
    return (n - nleft);
}

struct policy {
    //operation string
    char op;
    //size of data to transfer
    int transfer_size;
};

void transfer_manager(int num_job, std::string ip) {
    if(num_job <= 0) return;
    std::vector<Job> jobs;
    mutex_job_queue.lock();
    for (int i = 0; i < num_job && job_queue.size() != 0; i++) {
        // add struct
        struct Job job = job_queue.front();
        job_queue.pop();
        jobs.push_back(job);
    }
    mutex_job_queue.unlock();

    mutex_count.lock();
    count_jobs += jobs.size();
    mutex_count.unlock();

    for (int i = 0; i < jobs.size(); ++i)
        send_jobs(ip, jobs[i]);
}


struct policy send_initiated_policy() {

    double local_predict_time = 0;
    double remote_predict_time = 0;
    int transfer_num = 0;
    struct policy send_initiated;
    local_predict_time = pending_jobs / throttling_value;
    remote_predict_time = r_pending_jobs / r_throttling_value;
    if (local_predict_time > remote_predict_time + diff_thres) {
        transfer_num = ceil((pending_jobs * r_throttling_value - r_pending_jobs * throttling_value) /
                            (r_throttling_value + throttling_value));
        send_initiated.op = 'T';
        send_initiated.transfer_size = transfer_num;
        return send_initiated;
    } else {
        send_initiated.op = 'N';
        send_initiated.transfer_size = 0;
        return send_initiated;
    }
}

struct policy receiver_initiated_policy() {
    int local_predict_time = 0;
    int remote_predict_time = 0;
    int transfer_num = 0;
    struct policy receive_initiated;

    local_predict_time = pending_jobs / throttling_value;
    remote_predict_time = r_pending_jobs / r_throttling_value;
    if (local_predict_time < remote_predict_time - diff_thres) {
        transfer_num = floor(-pending_jobs * r_throttling_value + r_pending_jobs * throttling_value) /
                       (r_throttling_value + throttling_value);
        receive_initiated.op = 'R';
        receive_initiated.transfer_size = transfer_num;
        return receive_initiated;
    } else {
        receive_initiated.op = 'N';
        receive_initiated.transfer_size = 0;
        return receive_initiated;
    }
}

struct policy receiver_initiated1_policy() {
//    int local_predict_time = 0;
//    int remote_predict_time = 0;
    int transfer_num = 0;
    struct policy receive_initiated;

//    local_predict_time = pending_jobs / throttling_value;
//    remote_predict_time = r_pending_jobs / r_throttling_value;
    if (pending_jobs < r_pending_jobs - 100) {
        transfer_num = floor(r_pending_jobs - pending_jobs);
        receive_initiated.op = 'R';
        receive_initiated.transfer_size = transfer_num;
        return receive_initiated;
    } else {
        receive_initiated.op = 'N';
        receive_initiated.transfer_size = 0;
        return receive_initiated;
    }
}

struct policy symmetric_initiated_policy() {
    struct policy determine_policy;
    determine_policy = send_initiated_policy();
    if (determine_policy.op == 'T') {
        determine_policy.transfer_size /= 2;
        return determine_policy;
    } else {
        determine_policy = receiver_initiated_policy();
        if (determine_policy.op == 'R') {
            determine_policy.transfer_size /= 2;
            return determine_policy;
        } else {
            // op == 'N'
            return determine_policy;
        }
    }
}

int adaptor_symmetric() {
    while (true) {
//        while(REMOTE) {
            struct policy determine_policy = symmetric_initiated_policy();
            int transfer_size;
            if (r_pending_jobs == 0 && job_queue.size() == 0) {
                int count = 5;
                while(count > 0){
                    if (r_pending_jobs != 0 || job_queue.size() != 0) {
                        count ++ ;
                    }
                    count --;
                    std::this_thread::sleep_for(std::chrono::milliseconds(network_interval));
                }
                if (self_ip.compare(small_ip) == 0) {
                    mutex_computed_jobs.lock();
                    while (computed_jobs.size() != 0) {
                        send_rst(big_ip, computed_jobs.front());
                        computed_jobs.pop();
                    }
                    mutex_computed_jobs.unlock();
                    printf("All computed jobs transmitted to big vm!!\n");// ?
                }
                return 0;
            } else {
                if (determine_policy.op == 'N') {
                    continue;
                } else if (determine_policy.op == 'T') {

                    transfer_size = determine_policy.transfer_size;
                    transfer_manager(transfer_size, remote_ip);
                }
                    //determine_policy.op == 'R'
                else {

                    transfer_size = determine_policy.transfer_size;
                    char request[20];
                    //
                    std::sprintf(request, "R %d", transfer_size);
                    send_cmd(remote_ip, request);
                }
            }
//            std::this_thread::sleep_for(std::chrono::milliseconds(adaptor_interval));
//        }
        std::this_thread::sleep_for(std::chrono::milliseconds(adaptor_interval));
    }
    return 0;
    //pthread_exit(NULL);
}

int adaptor_send() {
    while (true) {
//        while(REMOTE) {
            struct policy determine_policy = send_initiated_policy();
            int transfer_size;
            if (r_pending_jobs == 0 && job_queue.size() == 0) {
                int count = 5;
                while(count > 0){
                    if (r_pending_jobs != 0 || job_queue.size() != 0) {
                        count ++ ;
                    }
                    count --;
                    std::this_thread::sleep_for(std::chrono::milliseconds(network_interval));
                }
                if (self_ip.compare(small_ip) == 0) {
                    mutex_computed_jobs.lock();
                    while (computed_jobs.size() != 0) {
                        send_rst(big_ip, computed_jobs.front());
                        computed_jobs.pop();
                    }
                    mutex_computed_jobs.unlock();
                    printf("All computed jobs transmitted to big vm!!\n");// ?
                }
                return 0;
            } else {
                if (determine_policy.op == 'N') {
                    continue;
                } else {

                    transfer_size = determine_policy.transfer_size / 2;
                    transfer_manager(transfer_size, remote_ip);
                }
                    //determine_policy.op == 'R'
            }
//            std::this_thread::sleep_for(std::chrono::milliseconds(adaptor_interval));
//        }
        std::this_thread::sleep_for(std::chrono::milliseconds(adaptor_interval));
    }
    return 0;
    //pthread_exit(NULL);
}

int adaptor_receive() {
    while (true) {
//        while(REMOTE) {
            struct policy determine_policy = receiver_initiated1_policy();
            int transfer_size;
            if (r_pending_jobs == 0 && job_queue.size() == 0) {
                int count = 5;
                while(count > 0){
                    if (r_pending_jobs != 0 || job_queue.size() != 0) {
                        count ++ ;
                    }
                    count --;
                    std::this_thread::sleep_for(std::chrono::milliseconds(network_interval));
                }
                if (self_ip.compare(small_ip) == 0) {
                    mutex_computed_jobs.lock();
                    while (computed_jobs.size() != 0) {
                        send_rst(big_ip, computed_jobs.front());
                        computed_jobs.pop();
                    }
                    mutex_computed_jobs.unlock();
                    printf("All computed jobs transmitted to big vm!!\n");// ?
                }
                return 0;
            } else {
                if (determine_policy.op == 'N') {
                    continue;
                } 
                                    //determine_policy.op == 'R'
                else {

                    transfer_size = determine_policy.transfer_size / 2;
                    char request[20];
                    //
                    std::sprintf(request, "R %d", transfer_size);
                    send_cmd(remote_ip, request);
                }
            }
//            std::this_thread::sleep_for(std::chrono::milliseconds(adaptor_interval));
//        }
        std::this_thread::sleep_for(std::chrono::milliseconds(adaptor_interval));
    }
    return 0;
    //pthread_exit(NULL);
}


int worker_thread() {
    try {
        while (1) {
            clock_t startcputime = clock();
            while (!job_queue.empty()) {

                mutex_job_queue.lock();
                for (int i = 0; i < JOB_LEN; ++i) {
                    for (int j = 0; j < 6000; ++j) {
//                        for (int t = 0; t < batch; ++t) {
                        job_queue.front().A[i] += 1.111111;
//                        }
                    }

//                    rst[job_queue.front().start + i] = job_queue.front().A[i];
                }
                mutex_computed_jobs.lock();
                computed_jobs.push(job_queue.front());
                mutex_computed_jobs.unlock();

                job_queue.pop();
                mutex_job_queue.unlock();

                clock_t endcputime = clock();
                double diffms = (endcputime - startcputime) / (CLOCKS_PER_SEC / 1000);
                if (diffms >= 1000 * throttling_value) {
                    std::this_thread::sleep_for(std::chrono::milliseconds((int) (1000 * (1 - throttling_value))));
                    startcputime = clock();
                    // be more precise if sleep for (diffms/throttling_value * (1 - throttling_value))
                }
                pending_jobs = job_queue.size();
            }

        }
    } catch (exception e) {
    }
    return 0;
    //pthread_exit(NULL);
}

//TO DO

int state_manager() {

    try {
        while (1) {
//            while(REMOTE) {
                string msg = "p " + std::to_string(pending_jobs);
                send_cmd(remote_ip, msg);

                msg = "v " + std::to_string(throttling_value);
                send_cmd(remote_ip, msg);
                std::this_thread::sleep_for(std::chrono::milliseconds(state_manager_interval));
//            TO DO
//            }
//            std::this_thread::sleep_for(std::chrono::milliseconds(state_manager_interval));
        }
    } catch (exception e) {

    }
    return 0;
}


int server() {

    int status, sockfd, byte_count;
    char buf[MAXBUFLEN];
    struct addrinfo hints, *server;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((status = getaddrinfo(NULL, "40000", &hints, &server)) != 0) {
        std::fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
    }

    if ((sockfd = socket(server->ai_family, server->ai_socktype, server->ai_protocol)) < 0) {
        std::fprintf(stderr, "socket error: %s\n", gai_strerror(sockfd));
    }

    if ((status = bind(sockfd, server->ai_addr, server->ai_addrlen)) < 0) {
        std::fprintf(stderr, "bind: %s\n", gai_strerror(status));
    }

    if ((status = listen(sockfd, 10)) < 0) {
        std::fprintf(stderr, "listen: %s\n", gai_strerror(status));
    }

    freeaddrinfo(server);
    int client_fd;
    struct sockaddr_storage client_addr;
    socklen_t addr_size;
    char s[INET6_ADDRSTRLEN]; // an empty string

    // Calculate the size of the data structure
    addr_size = sizeof client_addr;

    std::printf("I am now accepting connections ...\n");

    try {

        // Accept a new connection and return back the socket desciptor
        while (1) {
            if ((client_fd = accept(sockfd, (struct sockaddr *) &client_addr, &addr_size)) < 0) {
                std::fprintf(stderr, "accept: %s\n", gai_strerror(client_fd));
                return -1;
            }

            inet_ntop(client_addr.ss_family, get_in_addr((struct sockaddr *) &client_addr), s, sizeof s);
//            std::printf("I am now connected to %s \n", s);

            REMOTE = client_fd >= 0 ? true : false;
            byte_count = recv(client_fd, buf, sizeof buf, 0);
            buf[byte_count] = '\0';
            std::string input = string(buf);

//            Change throttling value
            if (input.substr(0, 1) == "t") {
                std::vector<string> strs = split(input, ' ');
                throttling_value = stod(strs[2]);

            } 
            else if (input.substr(0, 1) == "v") {
                std::vector<string> strs = split(input, ' ');
                r_throttling_value = stod(strs[1]);

            } else if (input.substr(0, 1) == "p") {
                std::vector<string> strs = split(input, ' ');
                r_pending_jobs = stoi(strs[1]);
            }
                // r_throttling_value and r_pending_jobs
            else if (input.substr(0, 1) == "R") {
                std::string ssize = input.substr(2);
                int size;
                size = std::atoi(ssize.c_str());

                transfer_manager(size, remote_ip);
//                if (self_ip.compare(small_ip) != 0) {
//                    transfer_manager(size, big_ip);
//                } else {
//                    transfer_manager(size, small_ip);
//                }
            }

            close(client_fd);
        }
    } catch (exception e) {


    }
    return 0;
//    pthread_exit(NULL);
}

int server_for_jobs() {

    int status, sockfd, byte_count;
    char buf[MAXBUFLEN];
    struct addrinfo hints, *server;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((status = getaddrinfo(NULL, "8888", &hints, &server)) != 0) {
        std::fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
    }

    if ((sockfd = socket(server->ai_family, server->ai_socktype, server->ai_protocol)) < 0) {
        std::fprintf(stderr, "socket error: %s\n", gai_strerror(sockfd));
    }

    if ((status = bind(sockfd, server->ai_addr, server->ai_addrlen)) < 0) {
        std::fprintf(stderr, "bind: %s\n", gai_strerror(status));
    }

    if ((status = listen(sockfd, 10)) < 0) {
        std::fprintf(stderr, "listen: %s\n", gai_strerror(status));
    }

    freeaddrinfo(server);
    int client_fd;
    struct sockaddr_storage client_addr;
    socklen_t addr_size;
    char s[INET6_ADDRSTRLEN]; // an empty string
    // Calculate the size of the data structure
    char buffer[10000];
    addr_size = sizeof client_addr;
    printf("I am now accepting connections ...\n");
    try {
        // Accept a new connection and return back the socket desciptor
        while (1) {
            if ((client_fd = accept(sockfd, (struct sockaddr *) &client_addr, &addr_size)) < 0) {

                std::fprintf(stderr, "accept: %s\n", gai_strerror(client_fd));
                return -1;
            }

            REMOTE = client_fd >= 0 ? true : false;

            //add while(true) and add std::
//            std::printf("I am now connected to %s \n", s);

            struct Job job;
            
            if (cir_read(client_fd, buffer, sizeof(buffer)) <= 0)  continue;
            memcpy(&job, buffer, sizeof(buffer));

            printf("number of bytes received  is: %d \n", sizeof job);

            mutex_job_queue.lock();
            job_queue.push(job);
            mutex_job_queue.unlock();
// revise suggestion: should continuously reading jobs while keeping connection
            close(client_fd);

        }
    } catch (exception e) {


    }
    return 0;
//    pthread_exit(NULL);
}

int server_rst() {
    int status, sockfd, byte_count;
    char buf[MAXBUFLEN];
    char buffer[10000];
    struct addrinfo hints, *server;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((status = getaddrinfo(NULL, "40010", &hints, &server)) != 0) {
        std::fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
    }

    if ((sockfd = socket(server->ai_family, server->ai_socktype, server->ai_protocol)) < 0) {
        std::fprintf(stderr, "socket error: %s\n", gai_strerror(sockfd));
    }

    if ((status = bind(sockfd, server->ai_addr, server->ai_addrlen)) < 0) {
        std::fprintf(stderr, "bind: %s\n", gai_strerror(status));
    }

    if ((status = listen(sockfd, 10)) < 0) {
        std::fprintf(stderr, "listen: %s\n", gai_strerror(status));
    }

    freeaddrinfo(server);
    int client_fd;
    struct sockaddr_storage client_addr;
    socklen_t addr_size;
    char s[INET6_ADDRSTRLEN]; // an empty string
    // Calculate the size of the data structure
    addr_size = sizeof client_addr;
    printf("I am now accepting connections ...\n");
    try {
        // Accept a new connection and return back the socket desciptor
        while (1) {
            if ((client_fd = accept(sockfd, (struct sockaddr *) &client_addr, &addr_size)) < 0) {

                std::fprintf(stderr, "accept: %s\n", gai_strerror(client_fd));
                return -1;
            }


            struct Job job;
            if (cir_read(client_fd, buffer, sizeof(buffer)) <= 0)  continue;
            memcpy(&job, buffer, sizeof(buffer));
            // if ((byte_count = read(client_fd, &job, sizeof job)) <= 0) continue;

//                for (int i = 0; i < 1024; ++i) printf("%f\n", job.A[i]);

            // printf("number of bytes received  is: %d \n", byte_count);
            // printf("number of bytes received  is: %d \n", sizeof job);

            mutex_computed_jobs.lock();
            computed_jobs.push(job);
            mutex_computed_jobs.unlock();
//            int idx = job.start;

//            for(int i = 0; i < JOB_LEN; ++i) rst[idx + i] = job.A[i];

            printf("number of r_rst bytes received  is: %d \n", sizeof job);
            close(client_fd);
        }
    } catch(exception e) {

    }
}
int demo() {
    string cmd = "";
    while (1) {
        getline(cin, cmd);
        if (cmd[0] == 't' && cmd[1] == ' ') {
            if (cmd[2] == 's')
                send_cmd(small_ip, cmd);
            else if (cmd[2] == 'b')
                send_cmd(big_ip, cmd);
                // add std
        } else if(cmd.substr(0, 4).compare("llen") == 0) {
            printf("local queue size is: %lu \n", job_queue.size());
        } else if(cmd.substr(0, 6).compare("localt") == 0) {
            printf("local threshold value is: %f \n", throttling_value);
        } else if(cmd.substr(0, 7).compare("remotet") == 0) {
            printf("Remote threshold value is: %f \n", r_throttling_value);
        } else if(cmd.substr(0, 6).compare("localp") == 0) {
            printf("local pending jobs are : %d \n", pending_jobs);
        } else if(cmd.substr(0, 7).compare("remotep") == 0) {
            printf("remote pending jobs are : %d \n", r_pending_jobs);
        } else if(cmd.substr(0, 3).compare("cpu") == 0) {
            printf("local cpu usg is : %f\n", currentCpuUsg);
        } else if(cmd.substr(0, 3).compare("rst") == 0) {
//
            mutex_computed_jobs.lock();
            printf("number of computed jobs is: %lu \n", computed_jobs.size());
            printf("size of job queue is: %lu\n", job_queue.size());
            while(computed_jobs.size() != 0) {
                int idx = computed_jobs.front().start;
                for(int i = 0; i < JOB_LEN; ++i) rst[idx + i] = computed_jobs.front().A[i];
                computed_jobs.pop();
            }
            mutex_computed_jobs.unlock();

            int num = 0;
            for (int i = 0; i < 1024 * 1024 * 4; ++i) {
               // printf("%f\n", rst[i]);
                if(rst[i] > 6666) num++;
            }
            printf("number of elems is : %d \n", num);
            printf("number of jobs transferred is: %d", count_jobs);
            exit(0);
        }
        else std::printf("invalid input command!");
    }
    return 0;
    //pthread_exit(NULL);
}
int hardware_monitor() {
    init_hard_ware_monitor();
    try {
        while (1) {
            currentCpuUsg = getCurrentValue();

            std::this_thread::sleep_for(std::chrono::milliseconds(monitor_interval));
        }
    } catch (exception e) {

    }
    return 0;
    //pthread_exit(NULL);
}


void init(std::string ip) {
    if(ip.compare(big_ip) == 0) {
        for (int i = 0; i < 1024 * 4; i++) {
            struct Job job;
            job.start = i * JOB_LEN;
            for (int j = 0; j < JOB_LEN; j++) job.A[j] = 1.111111;
            job_queue.push(job);
        }
        printf("total jobs are: %lu\n", job_queue.size());
    }

}



int main() {


    REMOTE = false;
    std::fill_n(rst, 1024 * 1024 * 4, 1.111111);
    self_ip = get_ip();
    if (self_ip.compare(small_ip) == 0) {
        remote_ip = big_ip;
    } else if (self_ip.compare(big_ip) == 0) {
        remote_ip = small_ip;
    } else {
        std::cout << "Error: neither vm is me!" << std::endl;
        exit(-1);
    }



    init(self_ip);  //only required for one node

    std::vector<std::thread> threads;


    threads.push_back(std::thread(server));
    threads.push_back(std::thread(server_for_jobs));

    string pol = "";


    printf("Policy? \n");
    getline(cin,pol);

    if(pol.compare("even") == 0)
        threads.push_back(std::thread(adaptor_symmetric));
    else if(pol.compare("send") == 0)
        threads.push_back(std::thread(adaptor_send));
    else if(pol.compare("recv") == 0)
        threads.push_back(std::thread(adaptor_receive));







    threads.push_back(std::thread(demo));

//    while(!REMOTE);

    threads.push_back(std::thread(worker_thread));
    threads.push_back(std::thread(state_manager));
    threads.push_back(std::thread(hardware_monitor));
    threads.push_back(std::thread(server_rst));

    // init and send half jobs to another node


    for(int i = 0; i < 8; i++) threads[i].join();



    //pthread_join()
    return 0;
}
