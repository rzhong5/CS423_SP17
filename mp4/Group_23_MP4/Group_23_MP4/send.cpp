//
// Created by lxb on 2017/4/18.
//
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string>
#include <queue>
#include <cstring>
#include <thread>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#define JOB_LEN 1024

struct Job{
    double A[1024];
    int start;
};
int send_cmd(std::string ip, std::string cmd) {
    int sockfd;
    struct addrinfo hints, *servinfo;
    int status;
    int numbytes;
    std::memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if ((status = getaddrinfo(ip.c_str(), "40000", &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return -1;
    }
    if ((sockfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol)) < 0) {
        fprintf(stderr, "send_cmd Error socket \n");
        return -1;
    }

    if((status = connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen)) < 0) {
        fprintf(stderr, "send_cmd Error connect \n");
        return -1;
    }
    if ((numbytes = send(sockfd, cmd.c_str(), cmd.length(), 0)) < 0) {
        fprintf(stderr, "send_cmd Error: server command \n");
        return -1;
    }
    freeaddrinfo(servinfo);
    close(sockfd);
    return 0;
}

int send_jobs(std::string ip, struct Job job) {
    int sockfd;
    struct addrinfo hints, *servinfo;
    int status;
    int numbytes;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((status = getaddrinfo(ip.c_str(), "8888", &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return -1;
    }

    if ((sockfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol)) < 0) {
        fprintf(stderr, "send_jobs Error socket \n");
        return -1;
    }

    if ((status = connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen)) < 0) {
        fprintf(stderr, "send_jobs Error connect \n");
        return -1;
    }

    if ((numbytes = write(sockfd, (char *)&job, sizeof(job)) < 0)) {
        fprintf(stderr, "send struct Error: server command \n");
        return -1;
    }
   // printf("sent job %d, total bytes are: %d \n", job.start, numbytes);
    printf("sent job %d \n", job.start);
    freeaddrinfo(servinfo);
    close(sockfd);
    return 0;
}

int send_rst(std::string ip, struct Job job) {
    int sockfd;
    struct addrinfo hints, *servinfo;
    int status;
    int numbytes;
    memset(&hints, 0, sizeof hints);
    struct Job container;

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;


    if ((status = getaddrinfo(ip.c_str(), "40010", &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return -1;
    }

    if ((sockfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol)) < 0) {
        fprintf(stderr, "send_jobs Error socket \n");
        return -1;
    }

    if ((status = connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen)) < 0) {
        fprintf(stderr, "send_jobs Error connect \n");
        return -1;
    }

    if ((numbytes = write(sockfd, (char *)&job, sizeof(job)) < 0)) {
        fprintf(stderr, "send struct Error: server command \n");
        return -1;
    }
//    printf("sent job %d, total bytes are: %d \n", job.start, numbytes);
    printf("sent rst !!!!!!!\n");
    freeaddrinfo(servinfo);
    close(sockfd);
    return 0;
}
