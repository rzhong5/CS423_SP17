#include "userapp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

static int buffer_size = 1024;

void dispList(FILE* fp, char* buf){
	fseek(fp,0,SEEK_SET);
	while(fgets(buf,buffer_size,fp) != NULL){
		printf("%s",buf);
	}
}

int main(int argc, char* argv[])
{
	pid_t pid = getpid();
	FILE* fp = fopen("/proc/mp1/status","r+");
	char buf[buffer_size];

	if(fp == NULL){
		printf("The module is not loaded\n");
		return -1;
	}	

	printf("initialized list\n");

	while(fgets(buf,buffer_size,fp) != NULL){
		printf("%s",buf);
		//printf("1st read: %lu bytes scanned:%s\n",strlen(buf),buf);
	}

	printf("\n");
	printf("register process %u\n",pid);
	sprintf(buf,"%u",pid);
	fputs(buf,fp);

	printf("current list\n");

	while(fgets(buf,buffer_size,fp) != NULL){
		printf("%s",buf);
		//printf("2nd read: %lu bytes scanned:%s\n",strlen(buf),buf);
	}

	system("sleep 5");
	printf("\n");
	printf("cpu time updated\n");

	fseek(fp,0,SEEK_SET);
	while(fgets(buf,buffer_size,fp) != NULL){
		printf("%s",buf);
		//printf("2nd read: %lu bytes scanned:%s\n",strlen(buf),buf);
	}


	printf("\n");
	printf("please enter a process:\n");
	fgets(buf,buffer_size,stdin);
	fputs(buf,fp);
	dispList(fp,buf);

	system("sleep 6");
	printf("\n");
	printf("cpu time updated\n");
	dispList(fp,buf);


	fclose(fp);	

	return 0;
}
