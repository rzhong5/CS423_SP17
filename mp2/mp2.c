#define LINUX

#include "mp2_given.h"
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/gfp.h>
#include <linux/slab.h>
#include <linux/list.h> 
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/spinlock.h>
#include <linux/timer.h>
#include <linux/kthread.h>
#include <linux/mutex.h>
#include <asm/uaccess.h>


MODULE_LICENSE("GPL");
MODULE_AUTHOR("23");
MODULE_DESCRIPTION("CS-423 MP2");

typedef struct mp2_task_struct{
	struct list_head list;
	struct task_struct* linux_task;
	struct timer_list wakeup_timer;
	unsigned int pid;
	unsigned int state;
	unsigned int period;
	unsigned int next_period;
	unsigned long cpu_time;
}mp2_task_struct;

static const unsigned int SLEEPING = 0;
static const unsigned int READY = 1;
static const unsigned int RUNNING = 2;
static struct proc_dir_entry *reg_dir;
static struct proc_dir_entry *status_entry;
static spinlock_t lock;
static struct kmem_cache *mp2_cache;
static struct task_struct *dispatcher;
static struct mp2_task_struct *current_mp2_task = NULL;
DEFINE_MUTEX(task_mutex);
LIST_HEAD(task_list);


struct mp2_task_struct* highestPrior(void){
	struct list_head* pos;
	struct mp2_task_struct *tmp, *sel = NULL;
	unsigned long flags;
	unsigned int invPrior = 0xffffffff;     //very big
	
	spin_lock_irqsave(&lock,flags);	
	list_for_each(pos,&task_list){
		tmp = list_entry(pos,struct mp2_task_struct,list);
		if(tmp->period < invPrior && tmp->state == READY){
			sel = tmp;
			invPrior = tmp->period;
		}
	}
	if(sel == NULL){
		printk("No task for now is ready!\n");
	}
	spin_unlock_irqrestore(&lock,flags);
	return sel;
}

//Set the scheduler according to the scheduling method and priority
static inline int set_scheduler(struct task_struct *task, int method, int priority){
	struct sched_param sparam;
	sparam.sched_priority = priority;
	return sched_setscheduler(task, method, &sparam);
}

int dispatch_thread(void* data){
  struct mp2_task_struct *sel;
	while(true) {
		
		set_current_state(TASK_INTERRUPTIBLE);
		schedule();

		if(kthread_should_stop()){
			return 0;
		}              //  catch remove module signal  

		sel = highestPrior();
		mutex_lock_interruptible(&task_mutex);
		if(sel == NULL) {    // if there is no ready task for scheduling
			//for the old running task (preempted task), the dispatching thread should execute the following code:
			if(current_mp2_task){
				set_scheduler(current_mp2_task->linux_task, SCHED_NORMAL, 0);
				current_mp2_task->state = READY;
			}
			current_mp2_task = NULL;
		}
		else if((current_mp2_task && current_mp2_task->period > sel->period) || !current_mp2_task){
			if(current_mp2_task) {
				current_mp2_task->state = READY;
				//for the old running task (preempted task), the dispatching thread should execute the following code:
				set_scheduler(current_mp2_task->linux_task, SCHED_NORMAL, 0);
      				set_task_state(current_mp2_task->linux_task, TASK_UNINTERRUPTIBLE);    //why uninterruptible?
			}
			sel->state = RUNNING;

			//For the preempting task
      wake_up_process(sel->linux_task);
			set_scheduler(sel->linux_task, SCHED_FIFO, 99);   //first wake up, and then be executed
			
      current_mp2_task = sel;
		}
		mutex_unlock(&task_mutex);
	}
	printk("dispatcher exit.\n");
	return 0;
}

void timer_handler(unsigned int task_pid) {

	struct list_head* pos;
	struct mp2_task_struct *wakeup_task;
	struct mp2_task_struct *tmp;
	unsigned long flags;

	spin_lock_irqsave(&lock, flags);

	list_for_each(pos,&task_list){
		tmp = list_entry(pos,struct mp2_task_struct,list);
		if(tmp->pid == task_pid){
			wakeup_task = tmp;
		}
	}

	if(wakeup_task->linux_task == NULL) {
		spin_unlock_irqrestore(&lock, flags);
		printk("timer of task %u failed with error.\n", task_pid);
		return;
	}
	
	if(wakeup_task->state == SLEEPING) {
		printk("task %u is ready!\n", task_pid);
		wakeup_task->state = READY;
	} else {
		spin_unlock_irqrestore(&lock, flags);
		printk("timer of %u failed with error!\n", task_pid);
		return;
	}
	spin_unlock_irqrestore(&lock, flags);
	wake_up_process(dispatch_thread);
	printk("timer of task %u expires!\n", task_pid);
}

// 0: no admission; 1: admision
int admission_control(unsigned int period, unsigned int cpu_time){
	// runtime is measured by average
	unsigned long flags;	
	struct mp2_task_struct *pos;

	spin_lock_irqsave(&lock,flags);
	unsigned int ratio = (1000 * cpu_time) / period;
	list_for_each_entry(pos, &task_list, list)
		ratio += (1000 * pos->cpu_time) / pos->period;
	spin_unlock_irqrestore(&lock,flags);

	return ratio < 693;
}

void registration(unsigned int pid,unsigned long period, unsigned long cpu_time){
	//printk("PID:%u, period:%u, computation time:%lu\n",pid,period,cpu_time);

	unsigned long flags;
	
	//init the mp2_task_struct
	struct mp2_task_struct* tmp = kmem_cache_alloc(mp2_cache, GFP_KERNEL);
	tmp->pid = pid;
	tmp->period = period;
	tmp->cpu_time = cpu_time;
	tmp->next_period = jiffies + msecs_to_jiffies(period);
	tmp->linux_task = find_task_by_pid(pid);
	tmp->state = SLEEPING;	
	INIT_LIST_HEAD(&tmp->list);
	//Initilize timer
	init_timer(&tmp->wakeup_timer);
	setup_timer(&tmp->wakeup_timer,timer_handler,pid);

	//Check whether admission control passes
	if( !admission_control(period, cpu_time) || !tmp->linux_task) {
		del_timer(&tmp->wakeup_timer);
		kmem_cache_free(mp2_cache,tmp);
		// spin_unlock_irqrestore(&lock, flags);     // where is lock function
		printk("admission_control or find_task_by_pid failed");
		return;
	}


	spin_lock_irqsave(&lock,flags);
	list_add_tail(&tmp->list, &task_list);
	spin_unlock_irqrestore(&lock,flags);

	printk("task %u registered!\n",pid);
}

void yielding(unsigned int pid){
	struct mp2_task_struct *mp2_task, *tmp;
	unsigned long flags;
	struct list_head *pos;

	spin_lock_irqsave(&lock, flags);
	list_for_each(pos,&task_list){
		tmp = list_entry(pos,struct mp2_task_struct,list);
		if(tmp->pid == pid){
			mp2_task = tmp;
		}
	}

	if(mp2_task == NULL){
		printk("The task does not exist.\n"); 
		return;
	}


	if(mp2_task->state == RUNNING){
		if(jiffies < mp2_task->next_period){
			mp2_task->state = SLEEPING;
			mod_timer(&mp2_task->wakeup_timer,jiffies+msecs_to_jiffies(mp2_task->period));
		}
		mp2_task->next_period += msecs_to_jiffies(mp2_task->period);
	}
	else {
		spin_unlock_irqrestore(&lock, flags);
		return;
	}

	set_task_state(mp2_task,TASK_UNINTERRUPTIBLE);
	spin_unlock_irqrestore(&lock, flags);
	wake_up_process(dispatcher);
	schedule();
}

void de_registration(unsigned int pid){
	struct list_head *pos, *n;
	struct mp2_task_struct *tmp;
	unsigned long flags;

	spin_lock_irqsave(&lock,flags);	
	list_for_each_safe(pos,n,&task_list){
		tmp = list_entry(pos,struct mp2_task_struct,list);
		if(tmp->pid == pid){
			list_del(&tmp->list);
			del_timer(&tmp->wakeup_timer);
			kmem_cache_free(mp2_cache,tmp);	
		}
	}
	spin_unlock_irqrestore(&lock,flags);

	printk("task %u de-registered!\n",pid);
}

static ssize_t mp2_read (struct file *file, char __user *buffer, size_t count, loff_t *data){ 

	ssize_t copied = 0; 
	char * buf = (char *) kmalloc(count,GFP_KERNEL);
	struct list_head* pos;
	struct mp2_task_struct* tmp;
	unsigned long flags;

	spin_lock_irqsave(&lock,flags);	
	list_for_each(pos,&task_list){
		tmp = list_entry(pos,struct mp2_task_struct,list);
		copied+=sprintf(buf+copied,"PID: %u , period: %u , computation time: %lu\n",tmp->pid,tmp->period,tmp->cpu_time);
	}
	spin_unlock_irqrestore(&lock,flags);

	buf[copied] = 0;
	if(*data >= copied){
		kfree(buf);
		return 0;
	}

	if(copy_to_user(buffer, buf, copied)){
		kfree(buf);
		printk("Error in read\n");
		return -EINVAL;
	}
	*data+=copied;

	kfree(buf);

	return copied;
} 

static ssize_t mp2_write (struct file *file, const char __user *buffer, size_t count, loff_t 
*data){ 

	char* buf;
	char command;
	unsigned int period;
	unsigned long cpu_time;
	unsigned int pid;

	buf = (char *) kmalloc(count,GFP_KERNEL);
	if(copy_from_user(buf,buffer, count)){
		kfree(buf);
		return -EFAULT;
	}	
	buf[count] = 0;
	command = buf[0];

	if(command == 'R'){
		sscanf(buf,"%c,%u,%u,%lu",&command,&pid,&period,&cpu_time);
		registration(pid,period,cpu_time);
	}
	else if(command == 'Y'){
		sscanf(buf,"%c,%u",&command,&pid);
		yielding(pid);
	}
	else if(command == 'D'){
		sscanf(buf,"%c,%u",&command,&pid);
		de_registration(pid);
	}
	else{
		printk("invalid command\n");
	}

	//printk("command %s\n",buf);	
	kfree(buf);	

	return count;
} 

struct file_operations mp2_fops = {
	.owner = THIS_MODULE,
	.read= mp2_read,
	.write= mp2_write,
};


#define DEBUG 1

// mp2_init - Called when module is loaded

int __init mp2_init(void)
{
	#ifdef DEBUG
	printk(KERN_ALERT "MP2 MODULE LOADING\n");
	#endif

	reg_dir = proc_mkdir("mp2",NULL);
	
	if(!reg_dir)
	{
		printk(KERN_INFO "directory creation failed\n");
		return -ENOMEM;
	}
	printk(KERN_INFO "proc dir successfully created\n");

	status_entry = proc_create("status",0666,reg_dir,&mp2_fops);

	if(!status_entry)
	{
		remove_proc_entry("mp2",NULL);		
		printk(KERN_INFO "proc entry creation failed\n");
		return -ENOMEM;
	}
	printk(KERN_INFO "file successfully created\n");

	//initialize spinlock
	spin_lock_init(&lock);
	printk(KERN_INFO "lock initialized\n");

	//initialize dispatching thread
	dispatcher = kthread_run(dispatch_thread,NULL,"dispatcher");

	//allocate cache
	mp2_cache = kmem_cache_create("mp2_task_struct",sizeof(struct mp2_task_struct),ARCH_MIN_TASKALIGN,SLAB_PANIC,NULL);

	printk(KERN_ALERT "MP2 MODULE LOADED\n");
	return 0;   
}

// mp2_exit - Called when module is unloaded
void __exit mp2_exit(void)
{
		
	struct list_head* pos, *n;
	struct mp2_task_struct* tmp;

	#ifdef DEBUG
	printk(KERN_ALERT "MP2 MODULE UNLOADING\n");
	#endif
	// Insert your code here ...

	remove_proc_entry("status",reg_dir);
	remove_proc_entry("mp2",NULL);

	
	mutex_destroy(&task_mutex);

	list_for_each_safe(pos,n,&task_list){
		tmp = list_entry(pos,mp2_task_struct,list);
		//printk(KERN_ALERT "Remove element %d: time %d\n",tmp->pid,tmp->cpu_time);				
		list_del(&tmp->list);
		del_timer(&tmp->wakeup_timer);
		//printk(KERN_ALERT "Node Deleted\n");	
		kmem_cache_free(mp2_cache,pos);	
	}


	list_del(&task_list);
	printk(KERN_INFO "list successfully destroyed\n");

	int ret = kthread_stop(dispatcher);
	if (ret != -EINTR)
		printk("Counter thread has stopped\n");

	printk(KERN_ALERT "MP2 MODULE UNLOADED\n");
}

// Register init and exit funtions
module_init(mp2_init);
module_exit(mp2_exit);
