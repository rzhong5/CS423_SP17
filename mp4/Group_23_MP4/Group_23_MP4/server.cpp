//
// Created by lxb on 2017/4/18.
//

#include <iostream>
#include<sys/socket.h>
#include<netdb.h>
#include<arpa/inet.h>
#include<pthread.h>

void *get_in_addr(struct sockaddr * sa)
{
    if(sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}
