#include <iostream>
#include <stdlib.h>
#include <boost/version.hpp>
#include <iomanip>
#include <queue>
#include <thread>
#include <boost/timer/timer.hpp>
#include <chrono>
#include <QTcpSocket>
#include <QTcpServer>
#include <QDebug>
#include <string>
#include <pthread.h>
#include <sys/time.h>
#include <event.h>
#include <stdio.h>

using namespace std;

#define job_len 1024


struct Job{
    vector<double> *A;
    int start;

    Job() {
        start = 0;
        double tmp[job_len];
        fill_n(tmp, job_len, 0);
        vector<double> vec (tmp, tmp + sizeof(tmp) / (sizeof(tmp[0])));
        A = &vec;
    }
};
queue<Job> job_queue;
double throttling_value = 0.7;
double rst[1024 * 1024 * 4];
mutex mutex_job_queue;
int batch = 1000;
QTcpSocket* tcpSocket; //send msg
QTcpSocket* tcpClient = new QTcpSocket(); //create TCP-based socket to receive msg
int remote_queue_size;
int T;
//server send msg to client

void connect_client() {
	QTcpServer *tcpServer = new QTcpServer();
	bool keepConnection = true;
	while(keepConnection) {
		tcpServer->listen(QHostAddress("172.22.154.47"), 6666);
		while (tcpServer->isListening())
		{
			tcpServer->waitForNewConnection();
			if ((tcpSocket = tcpServer->nextPendingConnection())) {
				std::cout << "Connected to small vm!" << std::endl;
				tcpSocket->flush();
			}
		}
    }
    close(tcpSocket);
	pthread_exit(NULL);
}

void connect_server() {
    tcpClient->connectToHost("172.22.154.48", 5555); //connect socket to server
    tcpClient->waitForConnected(); //wait 
    tcpClient->waitForReadyRead(); 
    qDebug() << tcpClient->readAll();
    bool keepConnection = true; 
    //
	while(tcpClient->canReadLine()) {
		QByteArray qb = tcpClient->readLine();
		string str = stdString(qb.constData(), qb.length());
		if (!strcmp(str.substr(0, 4), "[ID]")) {
			std::cout << "Job received: ID is " << str << std::endl;
			int ID = atoi(str.substr(4).c_str());
			struct Job job;
			job.start = ID;
			job_queue.push(job);
		}
		else if (!strcmp(str.substr(0, 6), "[SIZE]")) {
			int SIZE = atoi(str.substr(6, 10).c_str());
			remote_queue_size = SIZE;
		}     		
	}
    pthread_exit(NULL);
    }

// 172.22.154.47 for big vm
// 172.22.154.48 for small vm

void init() {
	pthread_t thread_server;
	pthread_t thread_client;

	int rc = pthread_create(&thread_server, NULL, connect_client, NULL);
		
      if (rc){
         std::cout << "Error:unable to create thread as server," << rc << std::endl;
         exit(-1);
      }

    int rc_ = pthread_create(&thread_client, NULL, connect_server, NULL);

      if (rc_){
         std::cout << "Error:unable to create thread as client," << rc_ << std::endl;
         exit(-1);
      }

    for(int i = 0; i < 1024 * 4; i++) {
        struct Job job;
        job.start = i * job_len;
        job_queue.push(job);
    }

}

void worker_thread() {
    try {
        while(1) {
            clock_t startcputime = clock();
            while(!job_queue.empty()) {
                for (int i = 0; i < job_len; ++i) {
                    for (int j = 0; j < 1000 / batch; ++j) {
// 6000/batch
                        mutex_job_queue.lock();
                        for (int t = 0; t < batch; ++t) (*job_queue.front().A)[i] += 1.111111;
                        mutex_job_queue.unlock();

                        clock_t endcputime = clock();
                        double diffms = (endcputime - startcputime) / (CLOCKS_PER_SEC / 1000);
                        if(diffms >= 1000 * throttling_value) {
                            this_thread::sleep_for(chrono::milliseconds(1000 * (1 - throttling_value)));
                            startcputime = clock();
                        }
        // this_thread::sleep_for(chrono::milliseconds(diffms * (1 - throttling_value)/ throttling_value));
                    }
                }
                mutex_job_queue.lock();
                job_queue.pop();
                mutex_job_queue.unlock();
            }
//            TO DO
        }

    } catch (exception e) {
    }

}

void hardware_monitor() {

}

//void helper() {
//    try {
//        while (1) {
//            this_thread::sleep_for(chrono::milliseconds(100));
//
//
//        }
//    } catch (exception e) {
//
//    }
//}

// 172.22.154.47 for big vm
// 172.22.154.48 for small vm

class transfer_manager {


void send_to_remote(int num_job) {
	for (int i = 0; i < num_job; i ++) {
		mutex_job_queue.lock();
		struct Job job = job_queue.pop();
		mutex_job_queue.unlock();
		char jobID[10];
	    sprintf(jobID, "[ID]%d", job.start);
	    string s = jobID;
		tcpSocket->write(s, sizeof(s));
		std::cout << "ID of job sent: " << job.start << std::endl;
}
}

void receive_from_remote(int jobID) {

	struct Job job_received;
	job_received.start = jobID;
	mutex_job_queue.lock();
	job_queue.push(job);
	mutex_job_queue.unlock();
	std::cout << "ID of job received: " << job.start << std::endl;
}
}

class state_manager {
	int local_queue_size = 0;
	int local_throttling_value = 1;

	void update_information() {
		if (job_queue.size() != local_queue_size) {
			local_queue_size = job_queue.size();
			local_throttling_value = throttling_value;
			//TO DO Hardware Monitor
			char queue_size_update[10];
	    	std::sprintf(queue_size_update, "[SIZE]%10d[THROTTLING]%5d", local_queue_size, local_throttling_value);
	    	string s = queue_size_date;
			tcpSocket->write(s, sizeof(s));
		｝
    } 

    state_manager() {
    	struct event ev;
    	struct timeval tv;

    	tv.tv_sec = 1;
    	tv.tv_usec = 0;

    	event_init();
    	evtimer_set(&ev, update_information, NULL);
    	event_add(&ev, &tv);
    	event_dispatch();

    	return 0;
    }
}



int main() {
    std::cout << "Hello, World!" << std::endl;
    fill_n(rst, 1024 * 1024 * 4, 1.111111);
    thread first();
    bool tuning = true;
    while (tuning) {
    	if (job_queue.size() > T) {

    	}
    }
    

    return 0;

}